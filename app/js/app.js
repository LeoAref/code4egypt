'use strict';
/* app */

var app = angular.module('app',['ngRoute']);

app.config(function($routeProvider){
   $routeProvider
       .when('/',{
           templateUrl:'partials/index.html',
           controller:'homeCtrl'
       })
       .when('/about',{
           templateUrl:'partials/about.html',
           controller:'aboutCtrl'
       })
       .when('/login',{
           templateUrl:'partials/login.html',
           controller:'contactCtrl'
       })
       .when('/register',{
           templateUrl:'partials/register.html',
           controller:'contactCtrl'
       })
       .when('/contact',{
           templateUrl:'partials/contact.html',
           controller:'contactCtrl'
       })
       .when('/startups',{
           templateUrl:'partials/startups.html',
           controller:'contactCtrl'
       })
       .when('/investor',{
           templateUrl:'partials/profile-investor.html',
           controller:'contactCtrl'
       })
       .when('/startup',{
           templateUrl:'partials/profile-startup.html',
           controller:'contactCtrl'
       })
       .when('/register-investor',{
           templateUrl:'partials/register-investor.html',
           controller:'contactCtrl'
       })
       .when('/register-startup',{
           templateUrl:'partials/register-startup.html',
           controller:'contactCtrl'
       })
       .when('/request',{
           templateUrl:'partials/request.html',
           controller:'contactCtrl'
       })
       .otherwise({
           redirectTo:'/'
       });
});